<?php
class StackTest extends CTestCase
{
    public function testPushAndPop()
    {


        $this->assertEquals( '1', Yii::app()->params['admin_rid']);
        $stack = [];
        $this->assertEquals(0, count($stack));
        array_push($stack, 'foo');
        $this->assertEquals('foo', $stack[count($stack)-1]);
        $this->assertEquals(1, count($stack));
        $this->assertEquals('foo', array_pop($stack));
        $this->assertEquals(0, count($stack));
    }
}
?>
